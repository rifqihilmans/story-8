from django.urls import path
from . import views

app_name = 'story8app'

urlpatterns = [
	path('', views.homepage, name='homepage'),
]