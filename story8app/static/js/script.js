$(document).ready(
    $('#book-form').submit(function (event) {
        event.preventDefault();
        const bookTitle = $('#search').val();
        console.log(bookTitle);
        $.ajax({
            method: 'GET',
            // Ganti API KEY dengan key pribadi
            url: "https://www.googleapis.com/books/v1/volumes?q=" + bookTitle,
            success: function (result) {
                let bookRow = $('#table');
                bookRow.empty();
                const resultList = result.items;
                for (let i = 0; i < resultList.length; i++) {
                    bookRow.append(
                        '<thead>' +
                        '<tr>' +
                            '<th scope="col" id="cover">Book Cover</th>' +
                            '<th scope="col" id="title">Book Title</th>' +
                            '<th scope="col" id="author">Book Author</th>' +
                        '</tr>'+
                        '</thead>'
                    );
                    for(i = 0; i < resultList.length ; i++){
                        $("table").append(
                        "<tbody>" +
                        "<tr>" +
                            "<th scope='row'>" + "<img src= '"+ result.items[i].volumeInfo.imageLinks.thumbnail + "'>" + "</td>" +
                            "<td>" + result.items[i].volumeInfo.title + "</td>" +
                            "<td>" + result.items[i].volumeInfo.authors + "</td>" +
                        "</tr>" +
                        "</tbody>"
                        );
                    }
                }
            }
        });
    })
);